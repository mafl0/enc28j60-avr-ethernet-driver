SRCDIR 	 = src
BUILDDIR = build
TESTDIR  = test
LIBDIR   = lib

MMCU     = atmega328p
FCPU     = 16000000
DEV      = /dev/ttyACM0
PROGRAMMER = arduino

CC	    =	avr-gcc
LD      = avr-ld
OBJCOPY = avr-objcopy
AR      = avr-ar

CFLAGS += -Iinclude
CFLAGS += -Lbuild/lib
CFLAGS += -mmcu=$(MMCU)
CFLAGS += -g
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Wpedantic

$(BUILDDIR):
	mkdir -p $@

$(BUILDDIR)/$(TESTDIR): $(BUILDDIR)
	mkdir -p $@

$(BUILDDIR)/$(LIBDIR):  $(BUILDDIR)
	mkdir -p $@

$(BUILDDIR)/$(LIBDIR)/%.o: %.c | $(BUILDDIR)/$(LIBDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BUILDDIR)/$(TESTDIR)/%.elf: $(TESTDIR)/%.c | $(BUILDDIR)/$(LIBDIR)/libuart.o $(BUILDDIR)/$(LIBDIR)/libenc28j60.o $(BUILDDIR)/$(TESTDIR)
	$(CC) $(CFLAGS) -l:libuart.o -l:libenc28j60.o -o $@ $<
$(BUILDDIR)/$(TESTDIR)/%.hex: $(BUILDDIR)/$(TESTDIR)/%.elf 
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

$(BUILDDIR)/$(LIBDIR)/libuart.o: $(TESTDIR)/libuart.c | $(BUILDDIR)/$(LIBDIR)
	$(CC) $(CFLAGS) -c -o $@ $<
$(BUILDDIR)/$(LIBDIR)/libuart.S: $(TESTDIR)/libuart.c | $(BUILDDIR)/$(LIBDIR)
	$(CC) $(CFLAGS) -S -o $@ $<
$(BUILDDIR)/$(LIBDIR)/libenc28j60.o: $(SRCDIR)/libenc28j60.c | $(BUILDDIR)/$(LIBDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: upload

upload-main: $(BUILDDIR)/$(TESTDIR)/main.hex
	avrdude -p $(MMCU) -c $(PROGRAMMER) -P $(DEV) -U flash:w:$<:i

clean:
	rm -rf $(BUILDDIR)
