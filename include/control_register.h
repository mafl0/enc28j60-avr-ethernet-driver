/**
 * Registers and bit field names for the Microchip ENC28J80 10BASE-T ethernet
 * controller.
 */

#ifndef __INCLUDE_ENC28J60_CONTROL_REGISTER__
#define __INCLUDE_ENC28J60_CONTROL_REGISTER__

/**
 * Common control registers across all banks
 */
#define EIE      0x1b   // Ethernet interrupt enable register
#define EIR      0x1c   // Ethernet interrupt request (flag) register
#define ESTAT    0x1d   // Ethernet status register
#define ECON2    0x1e   // Ethernet control register 2
#define ECON1    0x1f   // Ethernet control register 1

/**
 * Common register bit field names
 */
// EIE
#define RXERIE   0
#define TXERIE   1
#define TXIE     3
#define LINKIE   4
#define DMAEI    5
#define PKTIE    6
#define INTIE    7

// EIR
#define RXERIF   0  
#define TXERIF   1  
#define TXIF     3
#define LINKIF   4  
#define DMAIF    5 
#define PKTIF    6 

// ESTAT
#define CLKRDY   0
#define TXABRT   1
#define RXBUSY   2
#define LATECOL  4
#define BUFER    6
#define INT      7

// ECON2
#define VRPS     3
#define PWRSV    5
#define PKTDEC   6
#define AUTOINC  7 

// ECON 1
#define BSEL0    0
#define BSEL     1
#define RXEN     2
#define TXRTS    3
#define CSUMEN   4
#define DMAST    5
#define RXRST    6
#define TXRST    7

/**
 * Control registers for bank 0
 */
#define ERDPTL   0x00  // Read pointer low byte
#define ERDPTH   0x01  // Read pointer high byte
#define EWRPTL   0x02  // Write pointer low byte
#define EWRPTH   0x03  // Write pointer high byte
#define ETXSTL   0x04  // TX start low byte
#define ETXSTH   0x05  // TX start high byte
#define ETXNDL   0x06  // TX end low byte
#define ETXNDH   0x07  // TX end high byte
#define ERXSTL   0x08  // RX start low byte
#define ERXSTH   0x09  // RX start high byte
#define ERXNDL   0x0a  // RX end low byte
#define ERXNDH   0x0b  // RX end high byte
#define ERXRDPTL 0x0c  // RX RD pointer low byte
#define ERXRDPTH 0x0d  // RX RD pointer high byte
#define ERXWRPTL 0x0e  // RX WR pointer low byte
#define ERXWRPTH 0x0f  // RX WR pointer high byte

#define EDMASTL  0x10  // DMA start low byte
#define EDMASTH  0x11  // DMA start high byte
#define EDMANDL  0x12  // DMA end low byte
#define EDMANDH  0x13  // DMA end high byte
#define EDMADSTL 0x14  // DMA destination low byte
#define EDMADSTH 0x15  // DMA destination high byte
#define EDMACSL  0x16  // DMA checksum low byte
#define EDMACSH  0x17  // DMA checksum high byte

/**
 * Control register bank 0 bit field names
 */
// ERDPTL
#define ERDPT0   0
#define ERDPT1   1
#define ERDPT2   2
#define ERDPT3   3
#define ERDPT4   4
#define ERDPT5   5
#define ERDPT6   6
#define ERDPT7   7
#define ERDPT8   0
#define ERDPT9   1
#define ERDPT10  2
#define ERDPT11  3
#define ERDPT12  4

// EWRPT
#define EWRPT0   0
#define EWRPT1   1
#define EWRPT2   2
#define EWRPT3   3
#define EWRPT4   4
#define EWRPT5   5
#define EWRPT6   6
#define EWRPT7   7
#define EWRPT8   0
#define EWRPT9   1
#define EWRPT10  2
#define EWRPT11  3
#define EWRPT12  4

// ETXSTL
#define ETXST0   0
#define ETXST1   1
#define ETXST2   2
#define ETXST3   3
#define ETXST4   4
#define ETXST5   5
#define ETXST6   6
#define ETXST7   7
#define ETXST8   0
#define ETXST9   1
#define ETXST10  2
#define ETXST11  3
#define ETXST12  4

// ETXST
#define ETXND0   0
#define ETXND1   1
#define ETXND2   2
#define ETXND3   3
#define ETXND4   4
#define ETXND5   5
#define ETXND6   6
#define ETXND7   7
#define ETXND8   0
#define ETXND9   1
#define ETXND10  2
#define ETXND11  3
#define ETXND12  4

// ERXST
#define ETXND0   0
#define ETXND1   1
#define ETXND2   2
#define ETXND3   3
#define ETXND4   4
#define ETXND5   5
#define ETXND6   6
#define ETXND7   7
#define ETXND8   0
#define ETXND9   1
#define ETXND10  2
#define ETXND11  3
#define ETXND12  4

// ERXNDL
#define ERXND0   0
#define ERXND1   1
#define ERXND2   2
#define ERXND3   3
#define ERXND4   4
#define ERXND5   5
#define ERXND6   6
#define ERXND7   7
#define ERXND8   0
#define ERXND9   1
#define ERXND10  2
#define ERXND11  3
#define ERXND12  4

// ERXRDPT
#define ERXRDPT0   0
#define ERXRDPT1   1
#define ERXRDPT2   2
#define ERXRDPT3   3
#define ERXRDPT4   4
#define ERXRDPT5   5
#define ERXRDPT6   6
#define ERXRDPT7   7
#define ERXRDPT8   0
#define ERXRDPT9   1
#define ERXRDPT10  2
#define ERXRDPT11  3
#define ERXRDPT12  4

// ERXWRPT
#define ERXWRPT0   0
#define ERXWRPT1   1
#define ERXWRPT2   2
#define ERXWRPT3   3
#define ERXWRPT4   4
#define ERXWRPT5   5
#define ERXWRPT6   6
#define ERXWRPT7   7
#define ERXWRPT8   0
#define ERXWRPT9   1
#define ERXWRPT10  2
#define ERXWRPT11  3
#define ERXWRPT12  4

// EDMAST
#define EDMAST0   0
#define EDMAST1   1
#define EDMAST2   2
#define EDMAST3   3
#define EDMAST4   4
#define EDMAST5   5
#define EDMAST6   6
#define EDMAST7   7
#define EDMAST8   0
#define EDMAST9   1
#define EDMAST10  2
#define EDMAST11  3
#define EDMAST12  4

// EDMAND
#define EDMAND0   0
#define EDMAND1   1
#define EDMAND2   2
#define EDMAND3   3
#define EDMAND4   4
#define EDMAND5   5
#define EDMAND6   6
#define EDMAND7   7
#define EDMAND8   0
#define EDMAND9   1
#define EDMAND10  2
#define EDMAND11  3
#define EDMAND12  4

// EDMADST
#define EDMADST0   0
#define EDMADST1   1
#define EDMADST2   2
#define EDMADST3   3
#define EDMADST4   4
#define EDMADST5   5
#define EDMADST6   6
#define EDMADST7   7
#define EDMADST8   0
#define EDMADST9   1
#define EDMADST10  2
#define EDMADST11  3
#define EDMADST12  4

// EDMACS
#define EDMACS0   0
#define EDMACS1   1
#define EDMACS2   2
#define EDMACS3   3
#define EDMACS4   4
#define EDMACS5   5
#define EDMACS6   6
#define EDMACS7   7
#define EDMACS8   0
#define EDMACS9   1
#define EDMACS10  2
#define EDMACS11  3
#define EDMACS12  4
#define EDMACS13  5
#define EDMACS14  6
#define EDMACS15  7

/*
 * Control registers for bank 1
 */
#define EHTR0    0x00 // Note the appended `R' because of shadowing of the bit field names
#define EHTR1    0x01 // Note the appended `R' because of shadowing of the bit field names
#define EHTR2    0x02 // Note the appended `R' because of shadowing of the bit field names
#define EHTR3    0x03 // Note the appended `R' because of shadowing of the bit field names
#define EHTR4    0x04 // Note the appended `R' because of shadowing of the bit field names
#define EHTR5    0x05 // Note the appended `R' because of shadowing of the bit field names
#define EHTR6    0x06 // Note the appended `R' because of shadowing of the bit field names
#define EHTR7    0x07 // Note the appended `R' because of shadowing of the bit field names
#define EPMMR0   0x08 // Note the appended `R' because of shadowing of the bit field names
#define EPMMR1   0x09 // Note the appended `R' because of shadowing of the bit field names
#define EPMMR2   0x0a // Note the appended `R' because of shadowing of the bit field names
#define EPMMR3   0x0b // Note the appended `R' because of shadowing of the bit field names
#define EPMMR4   0x0c // Note the appended `R' because of shadowing of the bit field names
#define EPMMR5   0x0d // Note the appended `R' because of shadowing of the bit field names
#define EPMMR6   0x0e // Note the appended `R' because of shadowing of the bit field names
#define EPMMR7   0x0f // Note the appended `R' because of shadowing of the bit field names
#define EPMCSL   0x10 // Note the appended `R' because of shadowing of the bit field names
#define EPMCSH   0x11 // Note the appended `R' because of shadowing of the bit field names

#define EPMOL    0x14
#define EPMOH    0x15

#define ERXFCON  0x18
#define EPKTCNT  0x19

/**
 * Control register bank 1 bit field names
 */

// EHT
#define EHT0  0
#define EHT1  1
#define EHT2  2
#define EHT3  3
#define EHT4  4
#define EHT5  5
#define EHT6  6
#define EHT7  7
#define EHT8  0
#define EHT9  1
#define EHT10 2
#define EHT11 3
#define EHT12 4
#define EHT13 5
#define EHT14 6
#define EHT15 7
#define EHT16 0
#define EHT17 1
#define EHT18 2
#define EHT19 3
#define EHT20 4
#define EHT21 5
#define EHT22 6
#define EHT23 7
#define EHT24 0
#define EHT25 1
#define EHT26 2
#define EHT27 3
#define EHT28 4
#define EHT29 5
#define EHT30 6
#define EHT31 7
#define EHT32 0
#define EHT33 1
#define EHT34 2
#define EHT35 3
#define EHT36 4
#define EHT37 5
#define EHT38 6
#define EHT39 7
#define EHT40 0
#define EHT41 1
#define EHT42 2
#define EHT43 3
#define EHT44 4
#define EHT45 5
#define EHT46 6
#define EHT47 7
#define EHT48 0
#define EHT49 1
#define EHT50 2
#define EHT51 3
#define EHT52 4
#define EHT53 5
#define EHT54 6
#define EHT55 7
#define EHT56 0
#define EHT57 1
#define EHT58 2
#define EHT59 3
#define EHT60 4
#define EHT61 5
#define EHT62 6
#define EHT63 7

// EPMM
#define EPMM0  0
#define EPMM1  1
#define EPMM2  2
#define EPMM3  3
#define EPMM4  4
#define EPMM5  5
#define EPMM6  6
#define EPMM7  7
#define EPMM8  0
#define EPMM9  1
#define EPMM10 2
#define EPMM11 3
#define EPMM12 4
#define EPMM13 5
#define EPMM14 6
#define EPMM15 7
#define EPMM16 0
#define EPMM17 1
#define EPMM18 2
#define EPMM19 3
#define EPMM20 4
#define EPMM21 5
#define EPMM22 6
#define EPMM23 7
#define EPMM24 0
#define EPMM25 1
#define EPMM26 2
#define EPMM27 3
#define EPMM28 4
#define EPMM29 5
#define EPMM30 6
#define EPMM31 7
#define EPMM32 0
#define EPMM33 1
#define EPMM34 2
#define EPMM35 3
#define EPMM36 4
#define EPMM37 5
#define EPMM38 6
#define EPMM39 7
#define EPMM40 0
#define EPMM41 1
#define EPMM42 2
#define EPMM43 3
#define EPMM44 4
#define EPMM45 5
#define EPMM46 6
#define EPMM47 7
#define EPMM48 0
#define EPMM49 1
#define EPMM50 2
#define EPMM51 3
#define EPMM52 4
#define EPMM53 5
#define EPMM54 6
#define EPMM55 7
#define EPMM56 0
#define EPMM57 1
#define EPMM58 2
#define EPMM59 3
#define EPMM60 4
#define EPMM61 5
#define EPMM62 6
#define EPMM63 7

// EPMCS
#define EPMCS0  0
#define EPMCS1  1
#define EPMCS2  2
#define EPMCS3  3
#define EPMCS4  4
#define EPMCS5  5
#define EPMCS6  6
#define EPMCS7  7
#define EPMCS8  0
#define EPMCS9  1
#define EPMCS10 2
#define EPMCS11 3
#define EPMCS12 4
#define EPMCS13 5
#define EPMCS14 6
#define EPMCS15 7

// EPMO
#define EPMO0  0
#define EPMO1  1
#define EPMO2  2
#define EPMO3  3
#define EPMO4  4
#define EPMO5  5
#define EPMO6  6
#define EPMO7  7
#define EPMO8  0
#define EPMO9  1
#define EPMO10 2
#define EPMO11 3
#define EPMO12 4

// ERXFCON
#define BCEN  0
#define MCEN  1
#define HTEN  2
#define MPEN  3
#define PMEN  4
#define CRCEN 5
#define ANDOR 6
#define UCEN  7

/**
 * Control registers for bank 2
 */
#define MACON1   0x00

#define MACON3   0x02
#define MACON4   0x03
#define MABBIPG  0x04

#define MAIPGL   0x06
#define MAIPGH   0x07
#define MACLCON1 0x08
#define MACLCON2 0x09
#define MAMXFLL  0x0a
#define MAMXFLH  0x0b

#define MICMD    0x12

#define MIREGADR 0x14

#define MIWRL    0x16
#define MIWRH    0x17
#define MIRDL    0x18
#define MIRDH    0x19

/**
 * Control register bank 2 bit field names
 */

// MACON1
#define MARXEN   0
#define PASSALL  1
#define RXPAUS   2
#define TXPAUS   3

// MACON3
#define FULDPX   0
#define FRMLNEN  1
#define HFRMEN   2
#define PHDREN   3
#define TXCRCEN  4
#define PADCFG0  5
#define PADCFG1  6
#define PADCFG2  7

// MACON4
#define NOBKOFF  4
#define BPEN     5
#define DEFER    6

// MABBIPG
#define BBIPG0 0
#define BBIPG1 1
#define BBIPG2 2
#define BBIPG3 3
#define BBIPG4 4
#define BBIPG5 5
#define BBIPG6 6

// MAIPGL
#define MAIPGL0 0
#define MAIPGL1 1
#define MAIPGL2 2
#define MAIPGL3 3
#define MAIPGL4 4
#define MAIPGL5 5
#define MAIPGL6 6

// MAIPGH
#define MAIPGH0 0
#define MAIPGH1 1
#define MAIPGH2 2
#define MAIPGH3 3
#define MAIPGH4 4
#define MAIPGH5 5
#define MAIPGH6 6

// MACLCON1
#define RETMAX0 0
#define RETMAX1 1
#define RETMAX2 2
#define RETMAX3 3

// MACLCON2
#define COLWIN0 0
#define COLWIN1 1
#define COLWIN2 2
#define COLWIN3 3
#define COLWIN4 4
#define COLWIN5 5

// MAMXFL
#define MAMXFL0  0
#define MAMXFL1  1
#define MAMXFL2  2
#define MAMXFL3  3
#define MAMXFL4  4
#define MAMXFL5  5
#define MAMXFL6  6
#define MAMXFL7  7
#define MAMXFL8  0
#define MAMXFL9  1
#define MAMXFL10 2
#define MAMXFL11 3
#define MAMXFL12 4
#define MAMXFL13 5
#define MAMXFL14 6
#define MAMXFL15 7

// MICMD
#define MIIRD   0
#define MIISCAN 1

// MIREGADR
#define MIREGADR0  0
#define MIREGADR1  1
#define MIREGADR2  2
#define MIREGADR3  3
#define MIREGADR4  4

// MIWR
#define MIWR0  0
#define MIWR1  1
#define MIWR2  2
#define MIWR3  3
#define MIWR4  4
#define MIWR5  5
#define MIWR6  6
#define MIWR7  7
#define MIWR8  0
#define MIWR9  1
#define MIWR10 2
#define MIWR11 3
#define MIWR12 4
#define MIWR13 5
#define MIWR14 6
#define MIWR15 7

// MIRD
#define MIRD0  0
#define MIRD1  1
#define MIRD2  2
#define MIRD3  3
#define MIRD4  4
#define MIRD5  5
#define MIRD6  6
#define MIRD7  7
#define MIRD8  0
#define MIRD9  1
#define MIRD10 2
#define MIRD11 3
#define MIRD12 4
#define MIRD13 5
#define MIRD14 6
#define MIRD15 7


// EBSTSD
#define EBSTSD0  0
#define EBSTSD1  1
#define EBSTSD2  2
#define EBSTSD3  3
#define EBSTSD4  4
#define EBSTSD5  5
#define EBSTSD6  6
#define EBSTSD7  7

// EBSTCON
#define BISTST   0
#define TME      1
#define TMSEL0   2
#define TMSEL1   3
#define PSEL     4
#define PSV0     5
#define PSV1     6
#define PSV2     7

// EBSTCS
#define EBSTCS0  0
#define EBSTCS1  1
#define EBSTCS2  2
#define EBSTCS3  3
#define EBSTCS4  4
#define EBSTCS5  5
#define EBSTCS6  6
#define EBSTCS7  7
#define EBSTCS8  0
#define EBSTCS9  1
#define EBSTCS10 2
#define EBSTCS11 3
#define EBSTCS12 4
#define EBSTCS13 5
#define EBSTCS14 6
#define EBSTCS15 7

// MISTAT
#define BUSY     0
#define SCAN     1
#define NVALID   2

// EREVID
#define EREVID0  0
#define EREVID1  1
#define EREVID2  2
#define EREVID3  3
#define EREVID4  4

// ECOCON
#define COCON0   0
#define COCON1   1
#define COCON2   2

// EFLOCON
#define FCEN0    0
#define FCEN1    1
#define FULDPXS  2

// EPAUS
#define EPAUS0  0
#define EPAUS1  1
#define EPAUS2  2
#define EPAUS3  3
#define EPAUS4  4
#define EPAUS5  5
#define EPAUS6  6
#define EPAUS7  7
#define EPAUS8  0
#define EPAUS9  1
#define EPAUS10 2
#define EPAUS11 3
#define EPAUS12 4
#define EPAUS13 5
#define EPAUS14 6
#define EPAUS15 7

/**
 * Control registers for bank 3
 */

#define MAADRR5  0x00 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define MAADRR6  0x01 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define MAADRR3  0x02 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define MAADRR4  0x03 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define MAADRR1  0x04 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define MAADRR2  0x05 // Note that an `R' is appended to the name of the register due to shadowing of the bit field names
#define EBSTSD   0x06
#define EBSTCON  0x07
#define EBSTCSL  0x08
#define EBSTCSH  0x09
#define MISTAT   0x0a

#define EREVID   0x12

#define ECOCON   0x15

#define EFLOCON  0x17
#define EPAUSL   0x18
#define EPAUSH   0x19

/**
 * Bit field names for bank 3
 */

// MAADR
#define MAADR0  0
#define MAADR1  1
#define MAADR2  2
#define MAADR3  3
#define MAADR4  4
#define MAADR5  5
#define MAADR6  6
#define MAADR7  7
#define MAADR8  0
#define MAADR9  1
#define MAADR10 2
#define MAADR11 3
#define MAADR12 4
#define MAADR13 5
#define MAADR14 6
#define MAADR15 7
#define MAADR16 0
#define MAADR17 1
#define MAADR18 2
#define MAADR19 3
#define MAADR20 4
#define MAADR21 5
#define MAADR22 6
#define MAADR23 7
#define MAADR24 0
#define MAADR25 1
#define MAADR26 2
#define MAADR27 3
#define MAADR28 4
#define MAADR29 5
#define MAADR30 6
#define MAADR31 7
#define MAADR32 0
#define MAADR33 1
#define MAADR34 2
#define MAADR35 3
#define MAADR36 4
#define MAADR37 5
#define MAADR38 6
#define MAADR39 7
#define MAADR40 0
#define MAADR41 1
#define MAADR42 2
#define MAADR43 3
#define MAADR44 4
#define MAADR45 5
#define MAADR46 6
#define MAADR47 7

#define COCON0 0
#define COCON1 1
#define COCON2 2


#endif
