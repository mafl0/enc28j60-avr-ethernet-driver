#ifndef __INCLUDE_ENC28J60__
#define __INCLUDE_ENC28J60__

#include "register.h"

/**
 * Commonly used macro for setting a byte value
 */
#ifndef _BV
#define _BV(x) (1 << x)
#endif

void SPISetup();

/**
 * Read control register
 */
uint8_t _RCR(uint8_t addr);

/**
 * Read Buffer Memory
 *
 * Copy buffer memory of size @len@ from the slave to the master. The result
 * will be saved in @buf@. Make sure that @buf@ is large enough to hold the data
 * coming back from the slave.
 */
void    _RBM(uint8_t * buf, size_t len);

/**
 * Write Control Register
 *
 * Set the value of a control register @addr@ to @val@.
 */
void    _WCR(uint8_t addr, uint8_t val);

/**
 * Write Buffer Memory
 *
 * Copy a buffer (@buf@) of size @len@ from the master to the to the buffer
 * memory of the slave. The internal write pointer of the ENC28j60 will wrap
 * around if @len@ is larger than the slave can accumulate. Therefore, if @len@
 * is too large, buffer memory will be circularly overwritten.
 */
void    _WBM(uint8_t * buf, size_t len);

/**
 * Bit field set
 *
 * Performs a bitwise OR between the current value of the control register
 * @addr@ and @val@.
 *
 * addr[n+1] = addr[n] | val
 */
void    _BFS(uint8_t addr, uint8_t val);

/**
 * Bit field clear
 *
 * Performs a bitwise NOTAND between the current value of the control register
 * @addr@ and @val@.
 *
 * addr[n+1] = addr[n] & ~val
 */
void    _BFC(uint8_t addr, uint8_t val);

/**
 * System Reset Command
 *
 * Soft reset the system and revert all settings to their defaults.
 */
void    _SRC();

#endif
