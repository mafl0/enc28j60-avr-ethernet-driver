#ifndef __INCLUDE_ENC28J60_INSTRUCTION__
#define __INCLUDE_ENC28J60_INSTRUCTION__

#include "enc28j60/include/spi.h"

typedef enum Opcode {
  RCR = 0, // Read control register
  RBM = 1, // Read buffer memory
  WCR = 2, // Write control register
  WBM = 3, // Write buffer memory
  BFS = 4, // Bit field set
  BFC = 5, // Bit field clear
  SRC = 7  // System reset command (soft reset)
} OpcodeT;

void _SPICommand(OpcodeT opcode, const uint8_t arg, uint8_t * data);
void readControlRegister(uint8_t reg);
  _SPICommand(RCR, 0x1f & reg, 0);
}


typedef enum MemBank {
  BANK0 = 0,
  BANK1 = 1,
  BANK2 = 2,
  BANK3 = 3
} MemBankT;

inline static void selectMemBank(MemBankT bank);

#endif
