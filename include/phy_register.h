/**
 * Registers and bit field names for the Microchip ENC28J80 10BASE-T ethernet
 * controller.
 */

#ifndef __INCLUDE_ENC28J60_PHY_REGISTER__
#define __INCLUDE_ENC28J60_PHY_REGISTER__

/**
 * PHY register addresses
 */

#define PHCON1  0x00  // PHY configuration register 1
#define PHSTAT1 0x01  // PHY status register 1
#define PHID1   0x02  // PHY identifier register 1
#define PHID2   0x03  // PHY identifier register 2
#define PHCON2  0x10  // PHY configuration register 2
#define PHSTAT2 0x11  // PHY status register 2
#define PHIE    0x12  // PHY interrupt enable register
#define PHIR    0x13  // PHY interrupt flag register
#define PHLCON  0x14  // PHY LED configuration register

/**
 * PHY bit field names
 */

// PHCON1
#define PDPXMD  8
#define PPWRSV  11
#define PLOOPBK 14
#define PRST    15

// PHSTAT1
#define JBSTAT  1
#define LLSTAT  2
#define PHDPX   11
#define PFDPX   12

// PHID1
#define OUI3    0 
#define OUI4    1 
#define OUI5    2 
#define OUI6    3 
#define OUI7    4 
#define OUI8    5 
#define OUI9    6 
#define OUI10   7 
#define OUI11   8 
#define OUI12   9 
#define OUI13   10
#define OUI14   11
#define OUI15   12
#define OUI16   13
#define OUI17   14
#define OUI18   15

// PHID2
#define PREV0   0
#define PREV1   1
#define PREV2   2
#define PREV3   3
#define PPN0    4
#define PPN1    5
#define PPN2    6
#define PPN3    7
#define PPN4    8
#define PPN5    9
#define OUI19   10
#define OUI20   11
#define OUI21   12
#define OUI22   13
#define OUI23   14
#define OUI24   15

// PHCON2
#define HDLDIS  8
#define JABBER  10
#define TXDIS   13
#define FRCLNK  14

// PHSTAT2
#define PLRITY  5 
#define DPXSTAT 9  
#define LSTAT   10
#define COLSTAT 11 
#define RXSTAT  12
#define TXSTAT  13

// PHIE
#define PGEIE   1
#define PLNKIE  4

// PHIR
#define PGIF    2
#define PLNKIF  4

// PHLCON
#define STRCH   1
#define LFRQ0   2
#define LFRQ1   3
#define LBCFG3  4
#define LBCFG2  5
#define LBCFG1  6
#define LBCFG0  7
#define LACFG3  8
#define LACFG2  9
#define LACFG1  10
#define LACFG0  11

#endif
