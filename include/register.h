/**
 * Registers and bit field names for the Microchip ENC28J80 10BASE-T ethernet
 * controller.
 */

#ifndef __INCLUDE_ENC28J60_REGISTER__
#define __INCLUDE_ENC28J60_REGISTER__

#include "control_register.h"
#include "phy_register.h"

#endif
