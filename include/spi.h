#ifndef __INCLUDE_ENC28J60_SPI__
#define __INCLUDE_ENC28J60_SPI__

extern void SPIWrite(uint8_t data);
extern uint8_t SPIRead();

#endif
