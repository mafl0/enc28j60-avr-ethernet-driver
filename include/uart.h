#ifndef __INCLUDE_UART_H__
#define __INCLUDE_UART_H__

void UARTSetup();
void UARTSendBuffer(char * buffer, size_t sz);

#endif
