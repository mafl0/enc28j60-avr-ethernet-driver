#include <stdint.h>
#include <stddef.h>

#include <avr/io.h>

#include "enc28j60.h"

#define DD_SS       DDB2
#define DD_MOSI     DDB3
#define DD_SCK      DDB5
#define SS_PIN      PORTB2

#define _SSH PORTB |=  _BV(SS_PIN)
#define _SSL PORTB &= ~_BV(SS_PIN)

#define BUSY_WAIT_FOR_COMPLETE_TX while (!(SPSR & _BV(SPIF)))
#define SPI_SND(x) \
  SPDR = x; \
  BUSY_WAIT_FOR_COMPLETE_TX
#define SPI_RCV \
  SPDR = 0x00; \
  BUSY_WAIT_FOR_COMPLETE_TX

void SPISetup() {
  DDRB |= _BV(DD_SS) | _BV(DD_MOSI) | _BV(DD_SCK); // SS, MOSI, SCK
  SPCR |= _BV(SPE) | _BV(MSTR) | _BV(SPR1) | _BV(SPR0);
  // MISO is automatically done by the avr
  _SSH;
}
typedef enum OpCode {
    RCR = 0 << 5
  , RBM = 1 << 5
  , WCR = 2 << 5
  , WBM = 3 << 5
  , BFS = 4 << 5
  , BFC = 5 << 5
  , SRC = 7 << 5
} OpCodeT;

uint8_t _RCR(uint8_t addr) {
  _SSL;
  SPI_SND(RCR | (addr & 0x1f));
  SPI_RCV;
  _SSH; 
  return SPDR;
}

void _RBM(uint8_t * buf, size_t len) {
  _SSL;
  SPI_SND(RBM | 0x1a);
  for (size_t i = 0; i < len; ++i) {
    SPI_RCV;
    buf[i] = SPDR;
  }
  _SSH; 
}

void _WCR(uint8_t addr, uint8_t val) {
  _SSL;
  SPI_SND(WCR | (addr & 0x1f));
  SPI_SND(val);
  _SSH; 
}

void _WBM(uint8_t * buf, size_t len) {
  _SSL;
  SPI_SND(WBM | 0x1a);
  for (size_t i = 0; i < len; ++i) {
    SPI_SND(buf[i]);
  }
  _SSH; 
}

void _BFS(uint8_t addr, uint8_t val) {
  _SSL;
  SPI_SND(BFS | (addr & 0x1f));
  SPI_SND(val);
  _SSH; 
}

void _BFC(uint8_t addr, uint8_t val) {
  _SSL;
  SPI_SND(BFC | (addr & 0x1f));
  SPI_SND(val);
  _SSH; 
}

void _SRC() {
  _SSL;
  SPI_SND(SRC | 0x1f);
  _SSH;
}
