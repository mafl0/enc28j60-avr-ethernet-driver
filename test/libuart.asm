
; UCSR0B |= _BV(UDRIE0)
sts 0x00, XH ; r16 = UCSR0B
sts 0xc1, XL
ld r16, X 
or r16 0x20  ; r16 = r16 | 0x20
st X, r16    ; UCSR0B = r16
