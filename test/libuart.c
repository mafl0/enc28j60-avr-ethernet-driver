#include <stddef.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "uart.h"

#define UART_QUEUE_SIZE 64

/**
 * Queue for data to be transmitted, with the pointers for the back and the
 * front of the queue.
 */
static char _uartTxQueue[UART_QUEUE_SIZE];
static size_t _uartTxQFront = 0
            , _uartTxQBack  = 0
            ;

/**
 * Setup the UART for transmission only. We need PORTD1 (TX) as an output, 
 * enable the UART transmitter (TXEN0) and set the character size to 8 bits
 * (UCSZ0[2:0]). At 16 MHz clock freq, 0x08 sets a baud rate of 115200.
 */
void UARTSetup() {
  PORTD  |= _BV(PORTD1); // PD1 output
  UCSR0B |= _BV(TXEN0);
  UCSR0C |= _BV(UCSZ01) | _BV(UCSZ00); // 
  UBRR0L  = 0x08; // baud = 115200_2N1
}

/**
 * If the UART data register is empty, load the next character from the buffer
 * if there is one. Note that we cannot use the USART_TX interrupt vector
 * because `TX complete' happens earlier than `UDRE', therefore aborting
 * transmissions randomly.
 */
ISR(USART_UDRE_vect) {
  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    if (_uartTxQFront != _uartTxQBack) {
      UDR0 = _uartTxQueue[_uartTxQFront];
      _uartTxQFront = (_uartTxQFront + 1) % UART_QUEUE_SIZE;
    } else
      UCSR0B &= ~_BV(UDRIE0);
  }
}

/**
 * Add an array of known size to the TX queue. this code cannot be interrupted
 * because it would cause `race conditions' on the queue pointers by the
 * interrupt handler and this function.
 */
void UARTSendBuffer(char * buffer, size_t sz) {
  ATOMIC_BLOCK(ATOMIC_FORCEON) {
    for (size_t i = 0; i < sz; ++i) {
      _uartTxQueue[_uartTxQBack] = buffer[i];
      _uartTxQBack = (_uartTxQBack + 1) % UART_QUEUE_SIZE;
    }
    UCSR0B |= _BV(UDRIE0);
  }
}

/*
uartsendbuffer

buffer
sz

loop init
i = 0


queue[end] = buffer[i]

sts endl, XL
sts endh, XH
ld r16,
end = end + 1 % size

sts 0x00, XH ; r16 = UCSR0b
sts 0xc1, XL
ld r16, X 
or r16 0x20  ; r16 = r16 | 0x20
st X, r16    ; UCSR0B = r16

store result to ucsr0b


*/

