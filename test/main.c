#include <stdint.h>
#include <stddef.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "uart.h" // for debugging messages

#include "enc28j60.h"

#define F_CPU 16000000UL
#include <util/delay.h>

void convertToHex(uint8_t v, char * str) {
  convertNibble((v & 0xf0) >> 4, str    );
  convertNibble( v & 0x0f      , str + 1);
}
void convertNibble(uint8_t n, char * c) {
  if (n < 0xa)
    *c = n + '0';
  else
    *c = n + 'a' - 0xa;
}

void _testRCR(uint8_t addr, uint8_t expectedValue, uint8_t mask) {
  uint8_t res;
  char res_str[2];
  res = _RCR(addr) & mask;
  convertToHex(res, res_str);
  if (res == expectedValue)
    UARTSendBuffer("Success\r\n", 9);
  else {
    UARTSendBuffer("Error: ", 7);
    UARTSendBuffer(res_str, 2);
    UARTSendBuffer("\r\n", 2);
  }
}

void _testWCR(uint8_t addr, uint8_t postVal) {
  uint8_t pre, post;
  pre = _RCR(addr);
  _WCR(addr, postVal);
  post = _RCR(addr);

  if (post == postVal)
    UARTSendBuffer("Success\r\n", 9);
  else {
    UARTSendBuffer("Error: ", 7);
    UARTSendBuffer(post, 1);
    UARTSendBuffer("\r\n", 2);
  }
}

int main(void) {
  UARTSetup();
  SPISetup();

  sei();

  uint8_t data[256];
  size_t len;

  /**
   * Test for reading a control register
   *
   * Read some control registers with known initial values and compare them.
   */
 
  _testRCR(EIE,    0x00, 0xff);
  _testRCR(EIR,    0x00, 0x7f);
  _testRCR(ESTAT,  0x00, 0xf7);
  _testRCR(ECON2,  0x80, 0xf8);
  _testRCR(ECON1,  0x00, 0xff);
  _testRCR(ERDPTL, 0xfa, 0xff);
  _testRCR(ERDPTH, 0x05, 0x1f);


  /**
   * Test for writing a control register
   *
   * Read a control register and then overwrite it with a different value. If
   * the value has changed to the different value then test passed. If value is
   * unchanged then test failed
   */



  while(1) {
  }
}
